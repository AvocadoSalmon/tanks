﻿using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    public int m_PlayerNumber = 1;       
    public Rigidbody m_Shell;            
    public Transform m_FireTransform;    
    public Slider m_AimSlider;
    public AudioSource m_ShootingAudio;  
    public AudioClip m_ChargingClip;     
    public AudioClip m_FireClip;         
    public float m_MinLaunchForce = 15f; 
    public float m_MaxLaunchForce = 30f; 
    public float m_MaxChargeTime = 0.75f;


    private TankMovement m_Movement;

    private string m_FireButton;         
    private float m_CurrentLaunchForce;  
    private float m_ChargeSpeed;         
    private bool m_Fired;


    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
        m_FireButton = "Fire" + m_PlayerNumber;

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;

        m_Movement = GetComponent<TankMovement>();
    }
    

    private void Update()
    {
        bool button = m_Movement.GetIsMouseMove() ? Input.GetMouseButton(0) : Input.GetButton(m_FireButton);
        bool buttonDown = m_Movement.GetIsMouseMove() ? Input.GetMouseButtonDown(0) : Input.GetButtonDown(m_FireButton);
        bool buttonUp = m_Movement.GetIsMouseMove() ? Input.GetMouseButtonUp(0) : Input.GetButtonUp(m_FireButton);

        //  スライダーの値を毎フレームリセット
        m_AimSlider.value = m_MinLaunchForce;

        //  最大チャージしていて、まだ発射していない時
        if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
        {
            m_CurrentLaunchForce = m_MaxLaunchForce;
            Fire();
        }
        else if (buttonDown) //  最初に発射ボタンを押した時
        {
            m_Fired = false;
            m_CurrentLaunchForce = m_MinLaunchForce;

            m_ShootingAudio.clip = m_ChargingClip;  //  チャージ音を設定
            m_ShootingAudio.Play();                 //  再生

        }
        else if (button && !m_Fired) //  未発射でボタンを押している間
        {
            m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime; //発射力が強化されていく

            m_AimSlider.value = m_CurrentLaunchForce;
        }
        else if (buttonUp && !m_Fired)   //  ボタンを離した時
        {
            Fire();
        }
    }


    private void Fire()
    {
        // 発射フラグON
        m_Fired = true;

        //  インスタンス生成とリジッドボディ化を同時に行う
        Rigidbody shellInstance = Instantiate( m_Shell, m_FireTransform.position, m_FireTransform.rotation )as Rigidbody;

        //  撃ったプレイヤー番号をShellExplosionに設定
        ShellExplosion shellExp = m_Shell.GetComponent<ShellExplosion>();
        shellExp.m_ShootPlayerNum = m_PlayerNumber;

        //  発射速度を設定
        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

        //  発射音を設定＆再生
        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();

        //  2連続で最大チャージで発射されない為にリセット
        m_CurrentLaunchForce = m_MinLaunchForce;

    }
}