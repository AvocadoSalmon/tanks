﻿using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    public float m_StartingHealth = 100f;          
    public Slider m_Slider;                        
    public Image m_FillImage;                      
    public Color m_FullHealthColor = Color.green;  
    public Color m_ZeroHealthColor = Color.red;    
    public GameObject m_ExplosionPrefab;
    

    private AudioSource m_ExplosionAudio;          
    private ParticleSystem m_ExplosionParticles;   
    private float m_CurrentHealth;  
    private bool m_Dead;            

 
    private void Awake()
    {
        //  爆発パーティクルの生成とオーディオのコンポーネント取得
        m_ExplosionParticles = Instantiate(m_ExplosionPrefab).GetComponent<ParticleSystem>();
        m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource>();

        m_ExplosionParticles.gameObject.SetActive(false);   //  爆発パーティクルを無効化
    }


    //  タンクが有効化されたら
    private void OnEnable()
    {
        m_CurrentHealth = m_StartingHealth;
        m_Dead = false;

        SetHealthUI();
    }

    public void TakeDamage(float amount)
    {
        m_CurrentHealth -= amount;

        SetHealthUI();  //  体力を更新

        if (m_CurrentHealth <= 0f && !m_Dead) //    タンクが生存中に体力が0になったら
        {

            OnDeath();  //  死亡！
        }
    }


    private void SetHealthUI()
    {
        // スライダーの値を設定
        m_Slider.value = m_CurrentHealth;

        // スライダーの値によって色を変える
        m_FillImage.color = Color.Lerp( m_ZeroHealthColor, m_FullHealthColor, m_CurrentHealth/ m_StartingHealth );
    }


    private void OnDeath()
    {
        m_Dead = true;  //  死亡フラグON

        //  爆発パーティクルをタンクの位置にして有効化
        m_ExplosionParticles.transform.position = transform.position;
        m_ExplosionParticles.gameObject.SetActive( true );

        m_ExplosionParticles.Play();    //  パーティクルを再生
        m_ExplosionAudio.Play();        //  爆発音を再生

        gameObject.SetActive(false);    //  タンクを無効化
    }

    public float GetHP()
    {
        return m_CurrentHealth;
    }
}