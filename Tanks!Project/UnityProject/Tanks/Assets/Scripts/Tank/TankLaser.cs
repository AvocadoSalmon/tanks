using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class TankLaser : MonoBehaviour
{
    public int m_PlayerNumber = 1;
    public Rigidbody m_Laser;
    public Transform m_LaserTransform;
    public Transform m_TurretTransform;


    public Ease m_Ease_Type;           //  イージングタイプ
    public float m_time = 0.5f;        //  イージングにかかる時間

    [SerializeField, Range(0f, 1f)]
    public float m_t = 0.1f;           //  両端の距離を１とした時の割合
    public int m_LaserMaxNum = 5;      //  1度に撃てるレーザーの最大数
    public float m_AngleByShot;        //  1回の発射毎に回転する角度
    public float m_Interval = 0.2f;    //  1回の発射毎の待機時間



    private string m_FireButton;       //  発射ボタン名
    private bool m_Fired;              //  発射されているかのフラグ
    private Vector3 m_Target;          //  ターゲット座標
    private float m_Radius = 1f;       //  レーザーの配置半径
    private LaserCoolDown m_CoolDown;  //  レーザーのクールタイム


    private void OnEnable()
    {
        m_Target = Vector3.zero;
        m_AngleByShot = 180 / m_LaserMaxNum;
    }


    private void Start()
    {
        m_FireButton = "Laser" + m_PlayerNumber;
        m_AngleByShot = 180 / m_LaserMaxNum;
        m_CoolDown = GetComponent<LaserCoolDown>();
    }


    private void Update()
    {
        bool button = Input.GetButton(m_FireButton);
        bool buttonDown = Input.GetButtonDown(m_FireButton);
        bool buttonUp = Input.GetButtonUp(m_FireButton);


        if (buttonDown) //  発射ボタンを押した時
        {
            if (m_CoolDown.GetCanShoot()) // クールタイムが終わっていれば
            {
                StartCoroutine(DelayFire(m_AngleByShot, m_Interval));
            }
        }
        else if (buttonUp && !m_Fired)   //  ボタンを離した時
        {
            m_Fired = false;
        }
    }


    private void Fire(float axis,int i)
    {
        // 発射フラグON
        m_Fired = true;

        //  撃ったプレイヤー番号をLaserHitEffectに設定
        LaserHitEffect laserHitEff = m_Laser.GetComponent<LaserHitEffect>();
        laserHitEff.m_LaserPlayerNum = m_PlayerNumber;

        //  パスの設定
        Vector3 mid = Vector3.Lerp(m_LaserTransform.position, m_Target, m_t);
        Vector3 vec = (mid + Vector3.right) - mid;
        vec.Normalize();
        Vector3 result = Quaternion.Euler(0, 0, axis * i) * vec;


        Vector3[] path = new Vector3[3];
        path[0] = this.transform.position;
        path[1] = mid + result * m_Radius * 5f;
        path[2] = m_Target;

        Vector3 vec2 = m_LaserTransform.right - m_LaserTransform.position;
        vec2.Normalize();
        Vector3 result2 = Quaternion.Euler(0, 0, axis * i) * vec2;


        //  インスタンス生成とリジッドボディ化を同時に行う
        Rigidbody laserInstance = Instantiate
            (
                m_Laser,
                m_LaserTransform.position + result2 * m_Radius,
                m_LaserTransform.rotation
                ) as Rigidbody;

        laserInstance.GetComponent<Laser>().m_path = path;
        laserInstance.GetComponent<Laser>().m_Ease_Type = m_Ease_Type;
        laserInstance.GetComponent<Laser>().m_time = m_time;
        
        laserInstance.GetComponent<AudioSource>().Play();
    }

    //  制御点の初期化
    public void SetLaserTarget(Vector3 targetPos)
    {
        m_Target = targetPos;
    }

    //  指定秒毎に角度をずらしてレーザーを発射
    IEnumerator DelayFire( float eulerAngle,float interval )
    {
        //  角度をずらすのにインデックスを利用するためiは１から
        for (int i = 1; i <= m_LaserMaxNum; i++)
        {
            Fire(eulerAngle, i);

            yield return new WaitForSeconds(interval);
        }

        //  クールタイムに入る
        m_CoolDown.ResetCount();         
        m_CoolDown.SetCanChoot(false);
    }
}