﻿using UnityEngine;

public class TankMovement : MonoBehaviour
{
    public int m_PlayerNumber = 1;         // プレイヤー番号。１の時は１Pを示す
    public float m_Speed = 12f;            // タンクの前進と後退のスピード
    public float m_TurnSpeed = 180f;       // タンクが１秒間に何度左右に回転するか
    public AudioSource m_MovementAudio;    // エンジン音用のオーディオソース
    public AudioClip m_EngineIdling;       // タンクが停止している時の音
    public AudioClip m_EngineDriving;      // タンクが動いている時の音
    public float m_PitchRange = 0.2f;      // エンジン音のピッチ変更の変化量
    public float m_TurreTurnSpeed = 540f;  //  マウス移動でのタンクの回転スピード
    public float m_MouseMoveSpeed = 1f;    //  マウス移動でのタンクの移動スピード



    private string m_MovementAxisName;     // 前後に移動するための入力軸
    private string m_TurnAxisName;         // 回転するための入力軸
    private Rigidbody m_Rigidbody;         // タンクが移動する為のRigidbody
    private float m_MovementInputValue;    // 現在の移動入力値
    private float m_TurnInputValue;        // 現在の回転入力値
    private float m_OriginalPitch;         // オーディオのピッチの初期値

    private bool m_isMouseMove;            // マウス移動フラグ(入力移動OFF)
    private Camera m_MainCamera;
    private Vector3 m_MousePosition;       //  マウスの現在座標
    private Vector3 m_PreMousePosition;    //  記録した時点のマウスの座標
    private bool m_MouseMoveStart;         //  移動の開始フラグ
    private bool m_IsMouseMoving;          //  移動中フラグ




    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }


    /// <summary>
    /// オブジェクトが有効になった時に呼ばれる
    /// </summary>
    private void OnEnable()
    {
        //  TankMovementが有効化したら物理演算の影響を受ける
        m_Rigidbody.isKinematic = false;

        // 入力値の初期化
        m_MovementInputValue = 0f;
        m_TurnInputValue = 0f;

        m_MouseMoveStart = false;
        m_MousePosition = transform.position; //  マウスの座標を初期化
    }

    /// <summary>
    /// オブジェクトが無効になった時に呼ばれる
    /// </summary>
    private void OnDisable()
    {
        // TankMovementが無効化したら物理演算の影響を受けないので停止する
        m_Rigidbody.isKinematic = true;
    }


    private void Start()
    {
        m_MainCamera = Camera.main;           //  メインカメラを設定
        m_MousePosition = transform.position; //  マウスの座標を初期化
        m_MouseMoveStart = false;
        m_IsMouseMoving = false;

        // 入力軸の名前をプレイヤー番号で設定する
        m_MovementAxisName = "Vertical" + m_PlayerNumber;
        m_TurnAxisName = "Horizontal" + m_PlayerNumber;

        // オーディオソースのピッチの初期値を保存
        m_OriginalPitch = m_MovementAudio.pitch;
    }


    private void Update()
    {
        // プレイヤーの入力を格納
        m_MovementInputValue = Input.GetAxis( m_MovementAxisName );
        m_TurnInputValue = Input.GetAxis(m_TurnAxisName);

        int leftClick = 0;     //   左クリック
        int rightClick = 1;    //   右クリック
        int middleClick = 2;   //   中クリック

        MoveToClickPoint(rightClick); //    右クリックで移動


        //  エンジン音を再生
        EngineAudio();
    }


    private void EngineAudio()
    {
        //  入力値が一定以下だったら停止状態なのでアイドリング音を鳴らす
        if (Mathf.Abs(m_MovementInputValue) < 0.1f && Mathf.Abs(m_TurnInputValue) < 0.1f)
        {
            //  間違った音が鳴っていたら正してランダムでピッチを変化させて再生
            if (m_MovementAudio.clip == m_EngineDriving)
            {
                m_MovementAudio.clip = m_EngineIdling;
                m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                m_MovementAudio.Play();
            }
        }
        else // 動いているので走行音を鳴らす
        {
            //  間違った音が鳴っていたら正してランダムでピッチを変化させて再生
            if (m_MovementAudio.clip == m_EngineIdling)
            {
                m_MovementAudio.clip = m_EngineDriving;
                m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                m_MovementAudio.Play();
            }
        }
    }


    private void FixedUpdate()
    {
        if (!m_isMouseMove) //  キーボード移動
        {
            // 移動と回転
            Move();
            Turn();
        }
        else // マウス移動
        {
            
        }

    }

    //  移動方式の設定
    public void SetIsMouseMove(bool flag)
    {
        m_isMouseMove = flag;
    }

    //  移動方式の取得
    public bool GetIsMouseMove()
    {
        return m_isMouseMove;
    }

    //--------------------------------------------------------------------------------------------
    //  キーボード移動
    //--------------------------------------------------------------------------------------------
    private void Move()
    {
        if (Mathf.Abs(m_MovementInputValue) < 0.1f && Mathf.Abs(m_TurnInputValue) < 0.1f)
        {
            m_Rigidbody.velocity = Vector3.zero;
        }
        else
        {
            // １秒間でm_Speed分移動
            Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;

            //  指定位置へ移動(現在座標+移動量)
            m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
        }
    }


    private void Turn()
    {
        if (Mathf.Abs(m_TurnInputValue) < 0.1f && Mathf.Abs(m_TurnInputValue) < 0.1f)
        {
            m_Rigidbody.angularVelocity = Vector3.zero;
        }
        else
        {
            // １秒間でm_TurnSpeed分回転
            float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;

            //  回転したクォータニオンをRigidbodyのクォータニオンと合成
            Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);
            m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
        }
    }

    //--------------------------------------------------------------------------------------------
    //  マウス移動
    //--------------------------------------------------------------------------------------------
    private void TurnTankTurret( Vector3 mousePos )
    {
        //  子オブジェクトの砲台を取得
        GameObject tankTurret = transform.Find( "TankRenderers/TankTurret" ).gameObject;

        //  タンクからマウス座標へのベクトルを求める
        Vector3 diff = mousePos - this.transform.position;

        //  左右判定に外積を使う
        Vector3 axis = Vector3.Cross( tankTurret.transform.forward, diff );

        //  内積で回転方向を決め左右180度ずつなので180で割って-１～１にする
        float angle = Vector3.Angle( tankTurret.transform.forward, diff ) * (axis.y < 0 ? -1 : 1) / 180;

        // １秒間でm_TurnSpeed分回転
        float turn = angle * m_TurreTurnSpeed * Time.deltaTime;

        //  砲台の向きをマウスの位置へ
        tankTurret.transform.Rotate(new Vector3( 0, turn, 0));
    }

    //  右クリックした地点に移動
    private void MoveToClickPoint(int buttonNum)
    {

        if (m_isMouseMove) //  マウス移動
        {
            //  レイを飛ばしてマウス座標を得る
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100f))
            {
                TurnTankTurret(hit.point);   //  砲台を回転
            }

            Vector3 velocity = Vector3.zero;

            //　移動の目的地と0.1mより距離がある時は速度を計算
            if (Vector3.Distance(transform.position, m_MousePosition) > 0.1f)
            {
                Vector3 moveDirection = (m_MousePosition - transform.position).normalized;
                velocity = new Vector3(
                                                moveDirection.x * m_MouseMoveSpeed,
                                                0f,
                                                moveDirection.z * m_MouseMoveSpeed
                                              );
                transform.LookAt(transform.position + new Vector3(moveDirection.x, 0, moveDirection.z));
            }
            else
            {
                velocity = Vector3.zero;
            }

            transform.position += velocity * Time.deltaTime;

            if (Input.GetMouseButtonDown(buttonNum))    //  マウス入力
            {
                if (!m_IsMouseMoving)
                {
                    m_MousePosition = hit.point;
                    m_MouseMoveStart = true;
                }
            }
        }
    }
}