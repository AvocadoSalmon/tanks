﻿using System;
using UnityEngine;

[Serializable]  //  MonoBehaviourを継承していないのでインスペクター表示に必要
public class TankManager
{
    public Color m_PlayerColor;            
    public Transform m_SpawnPoint;         
    [HideInInspector] public int m_PlayerNumber;
    [HideInInspector] public string m_PlayerName = string.Empty;             
    [HideInInspector] public string m_ColoredPlayerText;
    [HideInInspector] public GameObject m_Instance;          
    [HideInInspector] public int m_Wins;                     


    private TankMovement m_Movement;       
    private TankShooting m_Shooting;
    private TankLaser m_Laser;
    private GameObject m_CanvasGameObject;
    private BillboardUI m_Billboard;



    public void Setup()
    {
        //  コンポーネントとオブジェクトの取得
        m_Movement = m_Instance.GetComponent<TankMovement>();
        m_Shooting = m_Instance.GetComponent<TankShooting>();
        m_CanvasGameObject = m_Instance.GetComponentInChildren<Canvas>().gameObject;
        m_Billboard = m_Instance.GetComponentInChildren<BillboardUI>();
        m_Laser = m_Instance.GetComponent<TankLaser>();

        //  プレイヤー番号設定
        m_Movement.m_PlayerNumber = m_PlayerNumber;
        m_Shooting.m_PlayerNumber = m_PlayerNumber;
        m_Laser.m_PlayerNumber = m_PlayerNumber;

        //  m_PlayerNameが既に設定されていたら
        if (m_PlayerName != string.Empty)
        {
            //  プレイヤーカーソル設定
            m_Billboard.SetPlayerName(m_PlayerName);

            //  プレイヤー毎に操作方法を設定
            if (m_PlayerName == "Player1") m_Movement.SetIsMouseMove( false );
            else if (m_PlayerName == "Player2") m_Movement.SetIsMouseMove( true );
        }
        else // 設定されてなければキーボード移動に設定
        {
            m_Movement.SetIsMouseMove(false);
        }

        //  16進数のRGBでプレイヤー番号を設定
        m_ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(m_PlayerColor) + ">PLAYER " + m_PlayerNumber + "</color>";

        //  タンクの各パーツのメッシュレンダラーを取得
        MeshRenderer[] renderers = m_Instance.GetComponentsInChildren<MeshRenderer>();

        //  上で設定したRGBでマテリアルの色をセット
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = m_PlayerColor;
        }
    }


    public void DisableControl()
    {
        m_Movement.enabled = false;
        m_Shooting.enabled = false;
        m_Laser.enabled = false;

        m_CanvasGameObject.SetActive(false);
    }


    public void EnableControl()
    {
        m_Movement.enabled = true;
        m_Shooting.enabled = true;
        m_Laser.enabled = true;

        m_CanvasGameObject.SetActive(true);
    }


    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);    //  勝者も全て一度オフにする
        m_Instance.SetActive(true);
    }

    //  自分以外のTankを探してレーザーターゲットに設定
    public void SetLaserTarget( Vector3 target )
    {
         m_Laser.SetLaserTarget( target );
    }
}
