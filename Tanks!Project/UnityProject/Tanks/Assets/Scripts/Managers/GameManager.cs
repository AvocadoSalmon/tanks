﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int m_NumRoundsToWin = 5;
    [SerializeField] private float m_StartDelay = 3f;
    [SerializeField] private float m_EndDelay = 5f;
    [SerializeField] private CameraControl m_CameraControl = default;
    [SerializeField] private Text m_MessageText = default;
    [SerializeField] private GameObject m_CommandCanvas = default;
    [SerializeField] private GameObject m_TankPrefab = default;
    [SerializeField] private TankManager[] m_Tanks = default;
    [SerializeField] private AudioClip m_RoundWinClip = default;
    [SerializeField] private AudioClip m_GameWinClip = default;
    [SerializeField] private AudioSource m_GameManagerSFXAudio = default;
    [SerializeField] private Text m_DisplayField = default;
    [SerializeField] private Text m_InputName = default;
    [SerializeField] private Text m_ScoreText = default;
    [SerializeField] private GameObject m_SendNameObj = default;
    [SerializeField] private GameObject m_RankingObj = default;



    private int m_RoundNumber;              
    private WaitForSeconds m_StartWait;     
    private WaitForSeconds m_EndWait;       
    private TankManager m_RoundWinner;
    private TankManager m_GameWinner;
    private bool m_WebRequestFlag;
    private bool m_ButtonOKFlag;
    private List<RankingData> _memberList;



    private void Start()
    {
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);
        m_ScoreText.text = "";
        m_WebRequestFlag = false;
        m_ButtonOKFlag = false;

        m_SendNameObj.SetActive(false);
        m_RankingObj.SetActive(false);

        SpawnAllTanks();
        SetCameraTargets();

        StartCoroutine(GameLoop());
    }

    private void SpawnAllTanks()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].m_Instance =
                Instantiate(m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation) as GameObject;
            m_Tanks[i].m_PlayerNumber = i + 1;
            m_Tanks[i].m_PlayerName = "Player" + m_Tanks[i].m_PlayerNumber.ToString();
            m_Tanks[i].m_Instance.name = m_Tanks[i].m_PlayerName;
            m_Tanks[i].Setup();
        }
    }


    private void SetCameraTargets()
    {
        Transform[] targets = new Transform[m_Tanks.Length];

        for (int i = 0; i < targets.Length; i++)
        {
            targets[i] = m_Tanks[i].m_Instance.transform;
        }

        m_CameraControl.m_Targets = targets;
    }


    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());    
        yield return StartCoroutine(RoundEnding());
        


        if (m_GameWinner != null)   //  ゲームの勝者が決定
        {
            yield return StartCoroutine(SendDataToRankingPhase());
            yield return StartCoroutine(DisplayDataFromRankingPhase());

            SceneManager.LoadScene(0);  //  新しいラウンド開始
        }
        else // ゲームの勝者がまだいない
        {
            StartCoroutine(GameLoop()); //  ループ継続
        }
    }


    private IEnumerator RoundStarting()
    {
        ResetAllTanks();
        DisableTankControl();

        TimerScript.m_Instance.SetActive(false);

        m_CameraControl.SetStartPositionAndSize();

        m_RoundNumber++;
        m_MessageText.text = "Round " + m_RoundNumber;

        yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
        EnableTankControl();

        TimerScript.m_Instance.SetActive(true);

        m_MessageText.text = string.Empty;

        while ( !OneTankLeft() )
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                //  レーザーのターゲットを設定
                SearchLaserTarget(i);
            }

            yield return null;
        }
    }


    private IEnumerator RoundEnding()
    {
        DisableTankControl();

        //  タイマー無効化
        TimerScript.m_Instance.SetActive(false);

        // タイマーリセット&総プレイ時間更新
        TimerScript.m_Instance.ResetTimer();

        //  デバッグ表示
        Debug.Log("総クリア時間(秒):" + TimerScript.m_Instance.GetTotalClearTime());

        m_RoundWinner = null;
        m_RoundWinner = GetRoundWinner();   //  ラウンド終了時にアクティブなタンクが勝者

        if (m_RoundWinner != null) m_RoundWinner.m_Wins++;

        m_GameWinner = GetGameWinner();

        string message = EndMessage();
        m_MessageText.text = message;

        yield return m_EndWait;
    }

    //---------------------------------------------------------------
    //  OKボタンを押した時の処理
    //---------------------------------------------------------------
    public void OnClickButtonOK()
    {
        m_ButtonOKFlag = true;
    }


    private bool OneTankLeft()
    {
        int numTanksLeft = 0;

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Instance.activeSelf)
                numTanksLeft++;
        }

        return numTanksLeft <= 1;
    }


    private TankManager GetRoundWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Instance.activeSelf)
                return m_Tanks[i];
        }

        return null;
    }


    private TankManager GetGameWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                return m_Tanks[i];
        }

        return null;
    }


    private string EndMessage()
    {
        string message = "DRAW!";

        if (m_RoundWinner != null)
        {
            message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

            //  ラウンド勝利SE
            m_GameManagerSFXAudio.clip = m_RoundWinClip;
            m_GameManagerSFXAudio.Play();

        }

        message += "\n\n\n\n";

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
        }

        if (m_GameWinner != null)
        {
            message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

            //  ゲーム勝利SE
            m_GameManagerSFXAudio.clip = m_GameWinClip;
            m_GameManagerSFXAudio.Play();
        }



        return message;
    }


    private void ResetAllTanks()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].Reset();
        }
    }


    private void EnableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].EnableControl();
        }

        //  操作できるので操作説明を出す
        m_CommandCanvas.SetActive(true);
    }


    private void DisableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].DisableControl();
        }

        //  操作できないので操作説明を消す
        m_CommandCanvas.SetActive(false);
    }

    //  自分以外のTankを探してレーザーターゲットに設定
    public void SearchLaserTarget( int self )
    {
        float minDistance = 100f;
        int nearId = -1;

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (self == i) continue;   //   自分とは比較しない
            //  自分以外のTankを発見
            float distance = Vector3.Distance
                (
                    m_Tanks[self].m_Instance.transform.position,
                    m_Tanks[i].m_Instance.transform.position
                    );

            //  前のデータと比較して距離が近い方に更新
            if (distance < minDistance)
            {
                minDistance = distance;
                nearId = i;
            }
        }
        //  最も近いTankの座標をレーザーターゲットに設定
        if (nearId != -1)
            m_Tanks[self].SetLaserTarget(m_Tanks[nearId].m_Instance.transform.position);

    }

    //*************************************************************************
    //
    //  データベース関連
    //
    //*************************************************************************

    /// <summary>
    /// Gets the json from www.
    /// </summary>
    private void GetJsonFromWebRequest()
    {
        // Wwwを利用して json データ取得をリクエストする
        StartCoroutine(
            DownloadJson(
                CallbackWebRequestSuccess, // APIコールが成功した際に呼ばれる関数を指定
                CallbackWebRequestFailed // APIコールが失敗した際に呼ばれる関数を指定
            )
        );
    }

    /// <summary>
    /// Callbacks the www success.
    /// </summary>
    /// <param name="response">Response.</param>
    private void CallbackWebRequestSuccess(string response)
    {
        //Json の内容を MemberData型のリストとしてデコードする。
        _memberList = RankingDataModel.DeserializeFromJson(response);

        //memberList ここにデコードされたメンバーリストが格納される。

        string sStrOutput = "";

        if (null == _memberList)
        {
            sStrOutput = "no list !";
        }
        else
        {
            //リストの内容を表示
            foreach (RankingData memberOne in _memberList)
            {
                sStrOutput += $"Name:{memberOne.Name} Score:{memberOne.Score}\n";
            }
        }

        m_DisplayField.text = sStrOutput;
    }

    /// <summary>
    /// Callbacks the www failed.
    /// </summary>
    private void CallbackWebRequestFailed()
    {
        // jsonデータ取得に失敗した
        m_DisplayField.text = "WebRequest Failed";
    }

    /// <summary>
    /// Downloads the json.
    /// </summary>
    /// <returns>The json.</returns>
    /// <param name="cbkSuccess">Cbk success.</param>
    /// <param name="cbkFailed">Cbk failed.</param>
    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/TankRankingSystem/TankRanking/getRanking");
        yield return www.SendWebRequest();

        if (www.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            // リクエスト成功の場合
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }

    //-------------------------------------------------------------------------------------------------------
    //  送信ボタンを押した時の処理
    //-------------------------------------------------------------------------------------------------------
    public void OnClickSetMessage()
    {
        //  名前入力UIを非表示
        m_SendNameObj.SetActive(false);
        //  ランキングUIを表示
        m_RankingObj.SetActive(true);


        m_DisplayField.text = "データ送信中...";

        SetJsonToWww();
    }

    //-------------------------------------------------------------------------------------------------------
    //  Jsonファイルをデータベースに送信する
    //-------------------------------------------------------------------------------------------------------
    private bool SetJsonToWww()
    {
        m_WebRequestFlag = true;

        return m_WebRequestFlag;
    }

    //-------------------------------------------------------------------------------------------------------
    //  指定データをデータベースに送信する
    //-------------------------------------------------------------------------------------------------------
    private IEnumerator SetMessage(string url, string name, int score, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("Name", name);
        form.AddField("Score", score);

        UnityWebRequest WebRequest = UnityWebRequest.Post(url, form);

        WebRequest.timeout = 10; //10秒経ったらタイムアウト

        //送信して結果が帰って来るまで待つ
        yield return WebRequest.SendWebRequest();

        if (null != WebRequest.error)
        {
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (WebRequest.isDone)
        {
            //送信成功
            Debug.Log($"Success:{WebRequest.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(WebRequest.downloadHandler.text);
            }
        }
    }

    //-------------------------------------------------------------------------------------------------------
    //  DisplayFIeldに結果を表示する
    //-------------------------------------------------------------------------------------------------------
    private void WebRequestSuccess(string response)
    {
        m_DisplayField.text = response;
    }

    //---------------------------------------------------------------
    //  名前とスコアをランキングに送信するフェイズ
    //---------------------------------------------------------------
    private IEnumerator SendDataToRankingPhase()
    {
        //  タンクの操作を無効化
        DisableTankControl();
        //  メッセージを非表示
        m_MessageText.gameObject.SetActive(false);
        //  タイマー無効化
        TimerScript.m_Instance.SetActive(false);
        //  スコアを計算して文字列で取得
        string scoreText = TimerScript.m_Instance.GetScore(m_RoundNumber).ToString();
        //  スコア表示を更新
        m_ScoreText.text = "スコア:" + scoreText;
        //  名前入力UIを表示
        m_SendNameObj.SetActive(true);
        //  ランキングUIを非表示
        m_RankingObj.SetActive(false);

        //  m_WebRequestFlagがtrueになるまで待つ
        yield return new WaitUntil(() => m_WebRequestFlag == true);

        //-------------------------------------------
        //以下m_WebRequestFlagがtrueの時
        //-------------------------------------------
        string sTgtURL = "http://localhost/TankRankingSystem/TankRanking/setRanking";

        string name = m_InputName.text;
        int score = TimerScript.m_Instance.GetScore(m_RoundNumber);

        yield return StartCoroutine(SetMessage(sTgtURL, name, score, WebRequestSuccess, CallbackWebRequestFailed));

    }

    //---------------------------------------------------------------
    //  ランキングからデータを受信して表示するフェイズ
    //---------------------------------------------------------------
    private IEnumerator DisplayDataFromRankingPhase()
    {
        //  タンクの操作を無効化
        DisableTankControl();
        //  メッセージを非表示
        m_MessageText.gameObject.SetActive(false);
        //  タイマー無効化
        TimerScript.m_Instance.SetActive(false);
        //  名前入力UIを非表示
        m_SendNameObj.SetActive(false);
        //  ランキングUIを表示
        m_RankingObj.SetActive(true);

        //リセット
        m_WebRequestFlag = false;


        GetJsonFromWebRequest();

        //  m_ButtonOKFlagがtrueになるまで待つ
        yield return new WaitUntil(() => m_ButtonOKFlag == true);
    }
}