using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserCoolDown : MonoBehaviour
{
    public Slider m_Slider;
    public const int m_StartingCount = 10;   //  何秒で再アクティブになるか
    public Text CountText;			        // 残り時間を表示するTextオブジェクト

    private int m_Count;
    private int m_Fill;
    private bool m_CanShoot;

    private float m_timer;    //  ただのタイマー

    //  タンクが有効化されたら
    private void OnEnable()
    {
        m_Count = m_StartingCount;
        m_Fill = m_StartingCount;
        CountText.text = "";
        m_CanShoot = true; ;

        // スライダーの値を設定
        m_Slider.value = m_Fill;
    }

    private void Update()
    {
        m_timer += Time.deltaTime;  //  タイマーを1秒に１ずつ増やす

        // 毎秒処理
        if (m_timer > 1f)
        {
            m_timer = 0f;
            if (!m_CanShoot) // クールタイム中
            {
                //Debug.Log("COUNT " + m_Count);
                //Debug.Log("FILL " + m_Fill);
                //Debug.Log("CANSHOOT " + m_CanShoot);
                if (m_Count > 0)
                {
                    m_Count--;

                    CountText.text = m_Count.ToString();
                }
                else
                {
                    m_CanShoot = true;  // リキャスト可能
                    m_Count = 0;


                    CountText.text = "";
                }
                if (m_Fill < m_StartingCount)
                {
                    m_Fill++;
                }
                else
                {
                    m_Fill = m_StartingCount;
                }
            }
        }

        SetLaserUI();
    }

    private void SetLaserUI()
    {
        // スライダーの値を設定
        m_Slider.value = m_Fill;
    }

    public int GetCount()
    {
        return m_Count;
    }

    public void ResetCount()
    {
        m_Count = m_StartingCount;
        m_Fill = 0;
        CountText.text = "";
    }

    public bool GetCanShoot()
    {
        return m_CanShoot;
    }

    public void SetCanChoot(bool canShoot)
    {
        m_CanShoot = canShoot;
    }
}
