using UnityEngine;
using DG.Tweening;

public class Laser : MonoBehaviour
{
    public Ease m_Ease_Type;  //  イージングタイプ
    public float m_time;      //  イージングにかかる時間

    public Vector3[] m_path = new Vector3[3];

    public AudioSource m_LaserAudio;
    public AudioClip m_LaserClip;



    void Start()
    {
        m_LaserAudio.clip = m_LaserClip;     //  発射音を設定
        
        transform.DOLocalPath(m_path, m_time, PathType.CatmullRom).SetEase(m_Ease_Type);
    }

    void Update()
    {

    }

    void FixedUpdate()
    {

    }

    void OnCollisionEnter()
    {

    }

    public void SetForwardAxis(Quaternion axis)
    {

    }
}