using UnityEngine;

public class LaserHitEffect : MonoBehaviour
{
    public LayerMask m_TankMask;
    public ParticleSystem m_LaserHitParticles;
    public AudioSource m_LaserHitAudio;
    public float m_MaxDamage = 5f;
    public float m_MaxLifeTime = 10f;
    public float m_ExplosionRadius = 1f;
    public int m_LaserPlayerNum = 0;  //　この弾を撃ったプレイヤー番号   

    void Start()
    {
        Destroy(m_LaserHitParticles.gameObject, m_MaxLifeTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Laser")) return;

        // 爆発範囲内のタンクを全て検索する
        Collider[] colliders = Physics.OverlapSphere
        (
            transform.position,
            m_ExplosionRadius,
            m_TankMask
            );

        bool bShootSelf = false;

        for (int i = 0; i < colliders.Length; i++)
        {
            //  当たったプレイヤー名と撃ったプレイヤー名が同じなら無視
            if (colliders[i].gameObject.name == "Player" + m_LaserPlayerNum)
            {
                bShootSelf = true;
                continue;
            }

            //  Rigidbodyが付いているか念の為チェック
            Rigidbody targetRigidbody = other.GetComponent<Rigidbody>();
            if (!targetRigidbody) continue;

            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
            if (!targetHealth) continue;  // TankHealthがなければ飛ばす

            float damage = CalculateDamage();
            targetHealth.TakeDamage(damage);  //  ダメージ！
        }

        if (bShootSelf) return;

        m_LaserHitParticles.transform.parent = null;   //  親子関係解除
        m_LaserHitParticles.Play();                    //  パーティクル再生
        m_LaserHitAudio.Play();                        //  爆発音再生

        //  パーティクルを指定時間で削除
        Destroy(m_LaserHitParticles.gameObject, m_LaserHitParticles.main.duration);
        Destroy(gameObject);  // レーザーを削除

    }

    private float CalculateDamage()
    {
        float damage = m_MaxDamage;

        damage = Mathf.Max( 0f, damage );   //  ダメージの値を0以上にする

        return damage;
    }


}