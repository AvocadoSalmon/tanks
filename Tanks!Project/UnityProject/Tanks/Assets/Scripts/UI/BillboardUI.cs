using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 常にカメラの方を向くオブジェクト回転をカメラに固定
/// </summary>
public class BillboardUI : MonoBehaviour
{
    public Sprite m_1pSprite;
    public Sprite m_2pSprite;

    private string m_PlayerName;
    private Image m_PlayerCursor;

    void Start()
    {
        //  プレイヤー名によって仕様スプライトを切り替える
        m_PlayerCursor = GetComponent<Image>();

        if (m_PlayerName == "Player1")
        {
            m_PlayerCursor.sprite = m_1pSprite;
        }
        else if (m_PlayerName == "Player2")
        {
            m_PlayerCursor.sprite = m_2pSprite;
        }
    }

    void LateUpdate()
    {
        // 回転をカメラと同期させる
        transform.rotation = Camera.main.transform.rotation;
    }

    //  タンク生成時にプレイヤー名を設定する
    public void SetPlayerName(string playerName)
    {
        m_PlayerName = playerName;
    }
}
