//*************************************************************************
//
//  ランキングの名前とスコアのデータを持つクラス
//
//*************************************************************************
public class RankingData
{
    public string Name { get; set; }
    public int Score { get; set; }

    public RankingData()
    {
        Name = "";
        Score = 0;
    }
}
