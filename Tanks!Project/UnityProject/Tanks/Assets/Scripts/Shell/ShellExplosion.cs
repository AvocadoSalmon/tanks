﻿using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask m_TankMask;
    public ParticleSystem m_ExplosionParticles;       
    public AudioSource m_ExplosionAudio;              
    public float m_MaxDamage = 100f;                  
    public float m_ExplosionForce = 1000f;            
    public float m_MaxLifeTime = 2f;                  
    public float m_ExplosionRadius = 5f;
    public int m_ShootPlayerNum = 0;  //　この爆発を起こした弾を撃ったプレイヤー番号            


    private void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        // 爆発範囲内のタンクを全て検索する
        Collider[] colliders = Physics.OverlapSphere( transform.position, m_ExplosionRadius, m_TankMask );

        for (int i = 0; i < colliders.Length; i++)
        {
            //  当たったプレイヤー名と撃ったプレイヤー名が同じならスキップ
            if(colliders[i].gameObject.name == "Player" + m_ShootPlayerNum) continue;

            //  Rigidbodyが付いているか念の為チェック
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
            if (!targetRigidbody) continue;

            //  爆発が当たったタンクに力を加える
            targetRigidbody.AddExplosionForce( m_ExplosionForce, transform.position, m_ExplosionRadius );

            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
            if (!targetHealth) continue;  // TankHealthがなければ飛ばす

            float damage = CalculateDamage(targetRigidbody.position);   //  位置によってダメージを計算
            targetHealth.TakeDamage( damage );  //  ダメージ！
        }

        m_ExplosionParticles.transform.parent = null;   //  親子関係解除
        m_ExplosionParticles.Play();                    //  パーティクル再生
        m_ExplosionAudio.Play();                        //  爆発音再生

        //  パーティクルを指定時間で削除
        Destroy( m_ExplosionParticles.gameObject, m_ExplosionParticles.main.duration );
        Destroy( gameObject );  //  砲弾を削除

    }


    private float CalculateDamage(Vector3 targetPosition)
    {
        Vector3 explosionToTarget = targetPosition - transform.position;    //  砲弾から当たったタンクへのベクトル

        float explosionDistance = explosionToTarget.magnitude;              //  ベクトルから距離を計算

        //  砲弾との距離が近い(relativeDistanceが大きい)ほどダメージが大きくなる
        float relativeDistance = (m_ExplosionRadius - explosionDistance) / m_ExplosionRadius;
        float damage = relativeDistance * m_MaxDamage;

        damage = Mathf.Max( 0f, damage );   //  ダメージの値を0以上にする

        return damage;
    }
}