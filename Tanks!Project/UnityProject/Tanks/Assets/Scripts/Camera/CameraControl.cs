﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float m_DampTime = 0.2f;                     //  カメラが指定位置に移動するまでの時間               
    public float m_ScreenEdgeBuffer = 4f;               //  タンクをカメラの端より少し内にするためのバイアス値       
    public float m_MinSize = 6.5f;                      //  カメラサイズの最小値（最大ズーム）        
    [HideInInspector] public Transform[] m_Targets;     //  カメラターゲットのトランスフォーム


    private Camera m_Camera;            //  カメラへの参照                  
    private float m_ZoomSpeed;          //  カメラがズームするスピード              
    private Vector3 m_MoveVelocity;     //  移動する速度          
    private Vector3 m_DesiredPosition;  //  カメラの理想座標(ここに向かってカメラが移動する)            


    private void Awake()
    {
        //  カメラリグの子オブジェクト（カメラ）のコンポーネントを取得
        m_Camera = GetComponentInChildren<Camera>();
    }


    private void FixedUpdate()
    {
        //  プレイヤーと同時に動かしたいのでFixedUpdate内で処理
        Move();
        Zoom();
    }


    private void Move()
    {
        FindAveragePosition();  //  プレイヤーの中間位置を探す

        //  理想の位置へスムーズに移動
        transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
    }


    private void FindAveragePosition()
    {
        Vector3 averagePos = new Vector3();
        int numTargets = 0;

        //  アクティブなタンクの座標を追加する
        for (int i = 0; i < m_Targets.Length; i++)
        {
            if (!m_Targets[i].gameObject.activeSelf)
                continue;

            averagePos += m_Targets[i].position;
            numTargets++;
        }

        //  中間座標を計算
        if (numTargets > 0)
            averagePos /= numTargets;

        averagePos.y = transform.position.y;        //  リジッドボディでタンクのY座標は動かないので固定

        m_DesiredPosition = averagePos;             //  理想位置をセット
    }


    private void Zoom()
    {
        float requiredSize = FindRequiredSize();    //  必要サイズを求める(距離÷アスペクト比)
        //  そのサイズへ向かって滑らかにズーム
        m_Camera.orthographicSize = Mathf.SmoothDamp(m_Camera.orthographicSize, requiredSize, ref m_ZoomSpeed, m_DampTime);
    }


    private float FindRequiredSize()
    {
        //  理想座標をローカル座標に変換
        Vector3 desiredLocalPos = transform.InverseTransformPoint(m_DesiredPosition);

        float size = 0f;

        for (int i = 0; i < m_Targets.Length; i++)
        {
            if (!m_Targets[i].gameObject.activeSelf)
                continue;

            //  カメラのターゲット座標をローカル座標に変換
            Vector3 targetLocalPos = transform.InverseTransformPoint(m_Targets[i].position);

            //  ローカル理想位置からローカルターゲット座標への距離を計算
            Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

            //  0と距離Yの絶対値を比較して大きい方をsizeにする
            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.y));

            //  Y方向のsizeと(距離Xの絶対値/アスペクト比=サイズ)を比較して大きい方をsizeにする
            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.x) / m_Camera.aspect);
        }
        
        size += m_ScreenEdgeBuffer;         //  調整バッファで少し余裕を持たせる

        size = Mathf.Max(size, m_MinSize);  //  sizeが最小値以下なら最小値で止める

        return size;
    }


    public void SetStartPositionAndSize()
    {
        FindAveragePosition();                          //  理想座標をを計算

        transform.position = m_DesiredPosition;         //  とりあえず最初は固定

        m_Camera.orthographicSize = FindRequiredSize(); //  初期サイズを計算して設定
    }
}