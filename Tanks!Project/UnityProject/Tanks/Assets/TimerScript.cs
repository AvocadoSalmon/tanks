using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
	[SerializeField] private int m_minute;
	[SerializeField] private float m_seconds;
	
	private float m_oldSeconds;	   //　前のUpdateの時の秒数
	private Text m_timerText;      //　タイマー表示用テキスト
	private float m_totalClearTime;//  総クリア時間(ドロー・負け含む)

	public static TimerScript m_Instance;

	private void Awake()
	{
		if (m_Instance == null)
		{
			m_Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}


	private void Start()
	{
		m_minute = 0;
		m_seconds = 0f;
		m_oldSeconds = 0f;
		m_totalClearTime = 0f;
		m_timerText = GetComponentInChildren<Text>();
		if(m_timerText)m_timerText.gameObject.SetActive(false);
	}

	private void Update()
	{
		if (this.gameObject.activeSelf)
		{
			m_seconds += Time.deltaTime;

			if (m_seconds >= 60f)
			{
				m_minute++;
				m_seconds = m_seconds - 60;
			}

			//　値が変わった時だけテキストUIを更新
			if ((int)m_seconds != (int)m_oldSeconds)
			{
				m_timerText.text = m_minute.ToString("00") + ":" + ((int)m_seconds).ToString("00");
			}


			m_oldSeconds = m_seconds;
		}
	}

	//-------------------------------------------------------------
	//	タイマーのアクティブ状態を設定する
	//-------------------------------------------------------------
	public void SetActive(bool active)
	{
		//	オブジェクトを有効にするか
		m_timerText.gameObject.SetActive(active);
	}

	//-------------------------------------------------------------
	//	タイマーをリセットする
	//-------------------------------------------------------------
	public void ResetTimer()
	{
		//	総クリア時間を更新
		m_totalClearTime += 60 * m_minute + m_seconds;

		m_minute = 0;
		m_seconds = 0f;
	}

	//-------------------------------------------------------------
	//	総プレイ秒数を取得して返す
	//-------------------------------------------------------------
	public float GetTotalClearTime()
	{
		return m_totalClearTime;
	}

	//-------------------------------------------------------------
	//	総プレイ秒数からスコアを算出
	//	roundNum:1ゲームのラウンド数
	//-------------------------------------------------------------
	public int GetScore(int roundNum)
	{
		//１セットの最大時間は60秒を想定。
		//60秒以上でスコア0になる。
		float diff = (60f * roundNum) - m_totalClearTime;
		if (0.0f >= diff) diff = 0.0f;
		int score = (int)Mathf.Floor(diff) * 10;

		Debug.Log("スコア"+ score.ToString() + "点");

		return score;
	}

}