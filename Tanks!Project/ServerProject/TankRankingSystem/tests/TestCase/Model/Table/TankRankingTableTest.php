<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TankRankingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TankRankingTable Test Case
 */
class TankRankingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TankRankingTable
     */
    public $TankRanking;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tank_ranking'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TankRanking') ? [] : ['className' => TankRankingTable::class];
        $this->TankRanking = TableRegistry::getTableLocator()->get('TankRanking', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TankRanking);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
