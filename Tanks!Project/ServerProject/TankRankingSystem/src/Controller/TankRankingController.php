<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TankRanking Controller
 *
 * @property \App\Model\Table\TankRankingTable $TankRanking
 *
 * @method \App\Model\Entity\TankRanking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TankRankingController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tankRanking = $this->paginate($this->TankRanking);

        $this->set(compact('tankRanking'));
    }

    /**
     * View method
     *
     * @param string|null $id Tank Ranking id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tankRanking = $this->TankRanking->get($id, [
            'contain' => []
        ]);

        $this->set('tankRanking', $tankRanking);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tankRanking = $this->TankRanking->newEntity();
        if ($this->request->is('post')) {
            $tankRanking = $this->TankRanking->patchEntity($tankRanking, $this->request->getData());
            if ($this->TankRanking->save($tankRanking)) {
                $this->Flash->success(__('The tank ranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tank ranking could not be saved. Please, try again.'));
        }
        $this->set(compact('tankRanking'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tank Ranking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tankRanking = $this->TankRanking->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tankRanking = $this->TankRanking->patchEntity($tankRanking, $this->request->getData());
            if ($this->TankRanking->save($tankRanking)) {
                $this->Flash->success(__('The tank ranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tank ranking could not be saved. Please, try again.'));
        }
        $this->set(compact('tankRanking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tank Ranking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tankRanking = $this->TankRanking->get($id);
        if ($this->TankRanking->delete($tankRanking)) {
            $this->Flash->success(__('The tank ranking has been deleted.'));
        } else {
            $this->Flash->error(__('The tank ranking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //-------------------------------------------------------------------------------------------------------------------------
    //  メッセージの取得
    //-------------------------------------------------------------------------------------------------------------------------
    public function getRanking(){
		error_log("getRanking()");
		//---------------
		// Viewのレンダーを無効化
		// これを行う事で対になるテンプレート(.tpl)が不要となる。
		$this->autoRender = false;

		//---------------
		// POSTデータの受け取り方
		// API呼び出しでPOSTパラメータが指定された場合は $this->request->data[] に入ってくる。
		// 下記例では id というパラメータが取得されたことを想定している。
		//$id = $this->request->data['id'];

		//---------------
		// DBからデータを読み込んで配列に変換
		//[Messageboard]テーブルからクエリを取得
		$query = $this->TankRanking->find('all');

		//※ここでクエリを利用してデータの並べ替えなどが行える。
//		debug($query);	//現在のクエリの状態をデバッグ表示する
//		$query->where(['id' => 1]);			//カラム['id']に[1]が入っているもののみに絞る。
//		$query->order(['id' => 'ASC']);		//カラム['id']をキーにして昇順ソート
//		$query->order(['id' => 'DESC']);	//カラム['id']をキーにして降順ソート
//		$query->limit(3);					//表示個数を3つに絞る

        //クエリー処理を行う。
        $query->order(['score'=>'DESC']);   //降順
        $query->limit(10);                  //取得件数を10件までに絞る

		//クエリを実行してarrayにデータを格納
		$json_array = json_encode($query);

		//---------------
		// $json_array の内容を出力
		echo $json_array;
	}

    //-------------------------------------------------------------------------------------------------------------------------
    //  メッセージをデータベースへ格納する
    //-------------------------------------------------------------------------------------------------------------------------

    public function setRanking()
    {
		error_log("setRanking()");
		//---------------
		// Viewのレンダーを無効化
		// これを行う事で対になるテンプレート(.tpl)が不要となる。
		$this->autoRender = false;

		//---------------
		// POSTデータの受け取り方
		// name,scoreをPOSTで受け取る。
       //POST パラメータを取得
       $postName   = $this->request->data["Name"];
       $postScore  = $this->request->data["Score"];

       //テーブルに追加するレコードを作る
       $record = array(
           "Name"=>$postName,
           "Score"=>$postScore,
           "Date"=>date("Y/m/d H:i:s")
       );

        //テーブルにレコードを追加
        $prm1    = $this->TankRanking->newEntity();
        $prm2    = $this->TankRanking->patchEntity($prm1,$record);
        if( $this->TankRanking->save($prm2) ){
            echo "success";	//success!
        }else{
            echo "failed";	//failed!
        }

	}

}
