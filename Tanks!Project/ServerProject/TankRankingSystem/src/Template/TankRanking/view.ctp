<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TankRanking $tankRanking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tank Ranking'), ['action' => 'edit', $tankRanking->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tank Ranking'), ['action' => 'delete', $tankRanking->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $tankRanking->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tank Ranking'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tank Ranking'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tankRanking view large-9 medium-8 columns content">
    <h3><?= h($tankRanking->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($tankRanking->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tankRanking->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($tankRanking->Score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($tankRanking->Date) ?></td>
        </tr>
    </table>
</div>
