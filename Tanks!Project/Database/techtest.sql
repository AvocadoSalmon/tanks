-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2021-10-21 03:34:11
-- サーバのバージョン： 10.4.17-MariaDB
-- PHP のバージョン: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `messageboard`
--

CREATE TABLE `messageboard` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Message` varchar(128) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `messageboard`
--

INSERT INTO `messageboard` (`Id`, `Name`, `Message`, `Date`) VALUES
(1, '朝', 'おはようございます', '2021-10-11 02:07:20'),
(2, '昼', 'こんにちは', '2021-10-11 02:07:20'),
(3, '夜', 'こんばんは', '2021-10-11 02:08:06'),
(4, 'test02', 'message', '2021-10-15 01:33:03'),
(9, 'テスト', 'ジンジャーエール', '2021-10-15 05:39:48'),
(17, 'すごいよ', '530', '2021-10-16 00:02:36'),
(18, '勇者', 'テスト', '2021-10-20 06:49:12'),
(19, '勇者', 'テスト', '2021-10-20 06:55:04');

-- --------------------------------------------------------

--
-- テーブルの構造 `onmyou`
--

CREATE TABLE `onmyou` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `ClearNum` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `onmyou`
--

INSERT INTO `onmyou` (`Id`, `Name`, `ClearNum`, `Date`) VALUES
(1, 'テスト', 1, '0000-00-00 00:00:00'),
(2, '勇者', 0, '2021-10-20 14:53:45');

-- --------------------------------------------------------

--
-- テーブルの構造 `ranking`
--

CREATE TABLE `ranking` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL DEFAULT '名無し',
  `ClearTime` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `tank_ranking`
--

CREATE TABLE `tank_ranking` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Score` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `tank_ranking`
--

INSERT INTO `tank_ranking` (`Id`, `Name`, `Score`, `Date`) VALUES
(1, '名無し', 0, '2021-10-16 02:58:46'),
(2, 'のび太', 0, '2021-10-16 03:02:00'),
(3, '夜神月', 200, '2021-10-16 03:02:22'),
(4, '坂田銀時', 150, '2021-10-16 03:02:44'),
(5, '江戸川コナン', 300, '2021-10-16 03:03:32'),
(6, '暁美ほむら', 270, '2021-10-16 03:03:49'),
(7, 'シャア・アズナブル', 285, '2021-10-16 03:04:05'),
(8, '御坂美琴', 243, '2021-10-16 03:04:24'),
(9, '竈門炭治郎', 144, '2021-10-16 03:04:49'),
(10, 'キルア・ゾルディック', 300, '2021-10-16 03:05:21'),
(11, 'ルフィ', 120, '2021-10-16 03:05:47'),
(12, 'ルルーシュ', 540, '2021-10-16 03:08:48'),
(13, '五条悟', 550, '2021-10-16 03:12:19'),
(14, '渚カヲル', 530, '2021-10-16 03:14:01'),
(15, 'ルパン三世', 540, '2021-10-16 03:23:17'),
(16, '胡蝶しのぶ', 530, '2021-10-16 03:23:42');

-- --------------------------------------------------------

--
-- テーブルの構造 `userdata`
--

CREATE TABLE `userdata` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Rank` int(11) NOT NULL,
  `Score` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `userdata`
--

INSERT INTO `userdata` (`Id`, `Name`, `Rank`, `Score`) VALUES
(1, 'ジャムおじさん', 159, 10264),
(2, 'Explosion!', 132, 6415),
(3, 'ひより＠周回中', 151, 15600),
(4, 'LXN', 175, 9875),
(5, 'みっちりぬこ丸', 64, 2500);

-- --------------------------------------------------------

--
-- テーブルの構造 `votesystem`
--

CREATE TABLE `votesystem` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Popularity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `votesystem`
--

INSERT INTO `votesystem` (`Id`, `Name`, `Popularity`) VALUES
(1, 'A', 3),
(2, 'B', 1),
(3, 'C', 2);

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `messageboard`
--
ALTER TABLE `messageboard`
  ADD PRIMARY KEY (`Id`) USING BTREE;

--
-- テーブルのインデックス `onmyou`
--
ALTER TABLE `onmyou`
  ADD PRIMARY KEY (`Id`);

--
-- テーブルのインデックス `tank_ranking`
--
ALTER TABLE `tank_ranking`
  ADD PRIMARY KEY (`Id`);

--
-- テーブルのインデックス `userdata`
--
ALTER TABLE `userdata`
  ADD PRIMARY KEY (`Id`);

--
-- テーブルのインデックス `votesystem`
--
ALTER TABLE `votesystem`
  ADD PRIMARY KEY (`Id`) USING BTREE;

--
-- ダンプしたテーブルの AUTO_INCREMENT
--

--
-- テーブルの AUTO_INCREMENT `messageboard`
--
ALTER TABLE `messageboard`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- テーブルの AUTO_INCREMENT `onmyou`
--
ALTER TABLE `onmyou`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- テーブルの AUTO_INCREMENT `tank_ranking`
--
ALTER TABLE `tank_ranking`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- テーブルの AUTO_INCREMENT `userdata`
--
ALTER TABLE `userdata`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- テーブルの AUTO_INCREMENT `votesystem`
--
ALTER TABLE `votesystem`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
